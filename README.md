# Dual Kosmo Bong0

This is a Kosmo dual drum module made using two [Nonlinear Circuits Bong0](https://www.nonlinearcircuits.com/modules/p/bong0) circuit boards. This repo contains KiCad design files and Gerber files for the Kosmo format panel, and notes on how to build this adaptation of the module.

## Current draw
8 mA +12 V, 8 mA -12 V


## Photos

![](Images/bong0.jpg)

![]()

## Documentation

* Panel layout: [front](Docs/Layout/bong0_panel/bong0_panel_F.SilkS,F.Mask.pdf), [back](Docs/Layout/bong0_panel/bong0_panel_B.SilkS,B.Mask.pdf)
* [Build notes](Docs/build.md)
<!-- * [Blog post]()-->

## Git repository

* [https://gitlab.com/rsholmes/bong0](https://gitlab.com/rsholmes/bong0)

