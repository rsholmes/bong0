# Dual Kosmo Bong0 build notes

To build this module, you will put two copies of the NLC PCB (meant for Eurorack) behind a Kosmo front panel. Making this idea work involves some rather non obvious procedures. Read the following suggested approach carefully, maybe a couple of times, before starting.

## Assemble the PCBs

The PCBs are assembled as described in NLC's build guide (note: In case it isn't clear from the build guide, their intention is that all components are mounted on the side of the PCB away from the panel aside from the 9 mm pots, the jacks, and the LED which are on the side facing the panel) except as follows:

* Jacks are not mounted to the PCBs
* LEDs are not mounted to the PCBs
* The trimmer potentiometers are mounted on the side facing the panel, opposite the side with most of the components. 
* Only one panel potentiometer is mounted to each PCB. On one board (call it `PCB 1`) it is the pot closest to the PCB end; on the other board (`PCB 2`) it is the other pot. **So the two PCBs are not assembled the same.**

## Mount panel components

1/4" jacks, LEDs, and the remaining two panel pots are front panel mounted. (The panel pots can be 9 mm or 16 mm. I used 9 mm pots that were designed for board mounting, with their standoffs snapped off and their terminals straightened, because I had no suitable 100k panel mount pots on hand.) 

## Mount PCBs

Each of the two PCBs is secured to the front panel using the board mounted pot and a 10 mm spacer. `PCB 1` is mounted on the right, as viewed from the back, with its power header at the bottom. `PCB 2` is mounted on the left, "upside down" with the power header at the top. The configuration is shown here (back view):

![](../Images/bong0_pcbs.png)

The spacers are secured with M3 screws going through the "X" holes in the `TRIG` jack footprints. The plating on the "X" holes connects to ground (as does the front panel) so it is OK to use metal screws. There are holes in the panel for screws to secure the other ends of the spacers.

## Wire panel components

The panel mounted components are wired to the corresponding footprint pads on the PCBs as shown here:

![](../Images/bong0_wiring.png)

It's easiest to solder wires to the PCBs before mounting them, and connect the other ends to the panel components after mounting the PCBs.

Connections are:

* `OUT` jacks (hole nearest "red" on silkscreen) to bottom jacks tip terminals (blue wires in drawing)
* `TRIG` jacks (hole nearest "red" on silkscreen) to middle jacks tip terminals (green wires)
* `CV` jacks (hole nearest op amp) to top jacks tip terminals (purple wires)
* `CV` jacks (hole on other side of "X" hole from the above) to top jacks sleeve terminals (black wires)
* LED footprints to LEDs (hole near flattened side of footprint to short lead (black wires), other hole to long lead (orange wires))
* Empty pot footprints to pots (note correct ordering of wires in drawing)

I wired the sleeve terminals of the bottom and middle jacks to those of the top jacks because I initially used a non conducting panel, but with an FR4 fabricated panel having a ground pour this shouldn't be necessary.

## Connect power

The two PCBs are electrically separate so you need to connect power to both. You could use two ribbon cables connecting to two bus board headers, but an alternative is to make a single ribbon cable with three IDC connectors, one at each end (connectors `A` and `C`) and one (connector `B`) on the cable near connector `A`; then plug `A` and `B` into the two PCBs and `C` into the bus board.

**Be very careful when plugging in the cable(s). There isn't room for box headers on the PCB, so it is easy to plug in a cable backwards, and with one of the PCBs mounted "upside down", *the headers are oriented oppositely:* On one the red stripe goes toward the top of the module, on the other it goes toward the bottom. See "RED" indication on the PCB silkscreen.**
